import React from 'react';
import Home from '../Page/Home';
import {
    BrowserRouter as Router,
    Switch,
    Route
} from 'react-router-dom';
import CovidInfo from '../Page/CovidInfo';
function App() {
    return (
        <Router>
            <Switch>
                <Route exact={true} path="/"  ><Home /></Route>
                <Route exact={true} path="/covid-info/:name" component={CovidInfo}></Route>


            </Switch>
        </Router>

    )
}
export default App;