import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
function Home() {
    const [countries, setCountries] = useState({});
    const [orignalcountries, setOrignalCountries] = useState({});
    const [loader, setLoader] = useState(false);

    const getAllCountries = () => {
        setLoader(true);
        axios.get("https://covid-api.mmediagroup.fr/v1/cases")
            .then((response) => {
                setCountries(response.data);
                setOrignalCountries(response.data);
                setLoader(false);
            })
            .catch((error) => {
                console.log(error);
                setLoader(false);
            })

    }

    const filter = (e) => {
        let searchKey = e.currentTarget.value.trim();
        if (searchKey) {
            let filteredCountries = Object.entries(orignalcountries).filter(([i, data]) => {
                return i.toLowerCase().includes(searchKey.toLowerCase())
            });
            const entries = new Map(filteredCountries);
            const obj = Object.fromEntries(entries);
            setCountries(obj);
        }
        else {
            setCountries(orignalcountries);
        }

    }



    useEffect(() => {
        getAllCountries();
    }, [])

    return (
        <div>
            {loader ?
                <div className="loading">Loading covid-info of all countries, Please wait...</div>
                :
                <div>
                    <div className="input-container">
                        <i className="fa fa-search icon" aria-hidden="true"></i>
                        <input type="text" className="search-bar" placeholder="Type here to search..." onInput={filter} />
                    </div>
                    <div>{Object.entries(countries) && Object.entries(countries).length > 0 ? <div> {Object.entries(countries) && Object.entries(countries).map(([i, data]) => (
                        <div key={i} className="country-name">
                            <div>
                                <Link className="countryN" to={"/covid-info/" + i}>{i}
                                </Link>
                            </div>
                            <div className="country-data">
                                <div className="case-title">
                                    <div>
                                        <i class="fa fa-bed" aria-hidden="true">
                                        </i>
                                    </div>
                                        Confirmed
                                    <div className="case-count">
                                        {data.All.confirmed}
                                    </div>
                                </div>
                                <div className="case-title">
                                    <div>
                                        <i class="fa fa-user-times" aria-hidden="true">
                                        </i>
                                    </div>
                                       Deaths
                                   <div className="case-count">
                                        {data.All.deaths}
                                    </div>
                                </div>
                                <div className="case-title">
                                    <div>
                                        <i class="fa fa-heartbeat" aria-hidden="true">
                                        </i>
                                    </div>
                                      Recovered
                                    <div className="case-count">
                                        {data.All.recovered}
                                    </div>
                                </div>
                            </div>
                        </div>
                    )
                    )
                    }
                    </div>
                        :
                        <div className="error-messege">Country Not Found...</div>}
                    </div>
                </div>
            }
        </div >
    )
}
export default Home;