import ReactDom from 'react-dom';
import React from 'react'
import App from './components/App';
import 'font-awesome/css/font-awesome.min.css';
import './Page/Home.css';
ReactDom.render(<App />, document.getElementById("root"));