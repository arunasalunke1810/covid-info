import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { Link, useParams } from 'react-router-dom';

function CovidInfo() {
    const [countries, setCountries] = useState({});
    const [orignalstate, setOrignalState] = useState({});
    const [loader, setLoader] = useState(false);

    let { name } = useParams();
    const getCovidDetails = () => {
        setLoader(true);
        axios.get("https://covid-api.mmediagroup.fr/v1/cases?country=" + name)
            .then((responce) => {
                setCountries(responce.data);
                setOrignalState(responce.data);
                setLoader(false);
                console.log(responce.data);
            }
            )
            .catch((error) => {
                console.log(error);
                setLoader(false);
            })

    }
    const filter = (e) => {
        let searchKey = e.currentTarget.value.trim();
        if (searchKey) {
            let filteredCountries = Object.entries(orignalstate).filter(([i, data]) => {
                return i.toLowerCase().includes(searchKey.toLowerCase())
            });
            const entries = new Map(filteredCountries);
            const obj = Object.fromEntries(entries);
            setCountries(obj);
        }
        else {
            setCountries(orignalstate);
        }

    }

    useEffect(() => {
        getCovidDetails();
    }, [])
    return (
        <div>
            {loader ? <div className="loading">Loading covid-info Of all states, Please wait...</div>
                :
                <div>
                    <Link to="/" className="back">Back</Link>
                    <div className="input-container">
                        <i className="fa fa-search icon" aria-hidden="true">
                        </i>
                    </div>
                    <input type="text" className="search-bar" placeholder="Type here to search..." onInput={filter} />
                    <div>{Object.entries(countries) && Object.entries(countries).length > 0 ? <div>{Object.entries && Object.entries(countries).map(([i, data]) =>
                        <div key={i} className="country-covid-details">{i}
                            <div className="country-data">
                                <div className="case-title">
                                    <div>
                                        <i class="fa fa-bed" aria-hidden="true">
                                        </i>
                                    </div>
                                   Comfirmed
                             <div className="case-count">
                                        {data.confirmed}
                                    </div>
                                </div>
                                < div className="case-title">
                                    <div>
                                        <i class="fa fa-user-times" aria-hidden="true">
                                        </i>
                                    </div>
                                     Deaths
                        <div className="case-count">
                                        {data.deaths}
                                    </div>
                                </div>
                                < div className="case-title" >
                                    <div>
                                        <i class="fa fa-heartbeat" aria-hidden="true">
                                        </i>
                                    </div>
                                    Recovered
                        <div className="case-count">
                                        {data.recovered}
                                    </div>
                                </div></div>

                        </div>)

                    }</div> : <div className="error-messege">State Not Found...</div>}</div></div>}

        </div >
    )
}
export default CovidInfo;